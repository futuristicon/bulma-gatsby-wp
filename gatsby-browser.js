// custom typefaces
import "typeface-montserrat"
import "typeface-merriweather"

// normalize CSS across browsers
import "./src/css/normalize.css"

// semantic-ui
// import "semantic-ui-css/semantic.min.css"

// custom CSS styles
// import "./src/css/style.css"

//Tailwind
import './src/styles/global.css'

//Font Awesome awesome import
import "./node_modules/@fortawesome/fontawesome-free/css/all.min.css"