import React from "react";
import { navigate } from "gatsby-link";

function encode(data: any) {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
}

const ContactForm = () => {
  const [state, setState] = React.useState({});

  const handleSubmit = (e: any) => {
    e.preventDefault();
    const form = e.target;
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": form.getAttribute("name"),
        ...state,
      }),
    })
      .then(() => navigate(form.getAttribute("action")))
      .catch((error) => alert(error));
  };

  const handleChange = (e: any) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  return (
    <form
      name="contact"
      method="post"
      action="/thanks-for-contacting-us/"
      data-netlify="true"
      data-netlify-honeypot="email-confirm"
      onSubmit={handleSubmit}
    >
      <div className="flex-auto p-5 lg:p-10">
        <h4 className="text-2xl font-semibold">
          Please fill a contact form below.
          <br />
          {/* Prefer emailing? Drop a message{" "}
          <a href="mailto:contact@futuristicon.com" className="underline">
            here
          </a>
          . */}
        </h4>
        {/* The following hidden input is for submitting without JavaScript enabled. Like we're ever gonna need it. */}
        <input type="hidden" name="contact" value="contact" />
        <div className="relative w-full mb-3 mt-8">
          <label
            className="block uppercase text-gray-700 text-xs font-bold mb-2"
            htmlFor="name"
          >
            Name*
          </label>
          <input
            type="text"
            className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
            placeholder="Full Name"
            name="name"
            id="name"
            style={{ transition: "all 0.15s ease 0s" }}
            onChange={handleChange}
            required
          />
        </div>
        <p hidden>
          <label>
            Don't fill this field
            <input name="email-confirm" onChange={handleChange} />
          </label>
        </p>
        <div className="relative w-full mb-3 mt-8">
          <label
            className="block uppercase text-gray-700 text-xs font-bold mb-2"
            htmlFor="company-name"
          >
            Company Name/Website
          </label>
          <input
            type="text"
            className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
            placeholder="(If applicable)"
            name="company-name"
            id="company-name"
            style={{ transition: "all 0.15s ease 0s" }}
            onChange={handleChange}
          />
        </div>
        <div className="relative w-full mb-3 mt-8">
          <label
            className="block uppercase text-gray-700 text-xs font-bold mb-2"
            htmlFor="phone-number"
          >
            Phone Number
          </label>
          <input
            type="tel"
            className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
            placeholder="+xxx"
            name="phone-number"
            id="phone-number"
            style={{ transition: "all 0.15s ease 0s" }}
            onChange={handleChange}
          />
        </div>
        <div className="relative w-full mb-3">
          <label
            className="block uppercase text-gray-700 text-xs font-bold mb-2"
            htmlFor="email"
          >
            Email*
          </label>
          <input
            className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
            placeholder="Email Address"
            style={{ transition: "all 0.15s ease 0s" }}
            type="email"
            name="email"
            id="email"
            onChange={handleChange}
            required
          />
        </div>
        <div className="relative w-full mb-3">
          <label
            className="block uppercase text-gray-700 text-xs font-bold mb-2"
            htmlFor="message"
          >
            Message*
          </label>
          <textarea
            rows={4}
            cols={80}
            className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
            placeholder="Type a message..."
            name="message"
            id="message"
            onChange={handleChange}
            required
          ></textarea>
        </div>
        <div className="text-center mt-6">
          <button
            className="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
            type="submit"
            style={{ transition: "all 0.15s ease 0s" }}
          >
            Send Message
          </button>
        </div>
      </div>
    </form>
  );
};

export default ContactForm;
