import React from "react";
import SubNavigationSection from "../sections/sub-navigation.section";

import StandardPageLayout from "../templates/standardPageLayout";

const AboutUsPage = () => {
  return (
    <StandardPageLayout>
      <SubNavigationSection
        heading="What We Do and Why We Do It"
        subHeading={
          <div className="italic">
            Learn about what we can do for you and what is our motive behind
            behind giving back a large chunk of our profit to life-extension
            advocators.
          </div>
        }
      />
      <section className="pb-20 bg-gray-300 -mt-24 xs:pt-32">
        <div className="container mx-auto px-4">
          <div className="flex flex-wrap">
            <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center xs:hidden">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                <div className="px-4 py-5 flex-auto">
                  <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                    <i className="fas fa-award"></i>
                  </div>
                  <h6 className="text-xl font-semibold">
                    Taking Care of Business
                  </h6>
                  <p className="mt-2 mb-4 text-gray-600">
                    End-to-end digital solution for your business - never have
                    to worry about development again.{" "}
                  </p>
                </div>
              </div>
            </div>
            <div className="w-full md:w-4/12 px-4 text-center xs:hidden">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                <div className="px-4 py-5 flex-auto">
                  <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                    <i className="fas fa-leaf"></i>
                  </div>
                  <h6 className="text-xl font-semibold">
                    Perfect for Your Company
                  </h6>
                  <p className="mt-2 mb-4 text-gray-600">
                  Our hybrid business model allows us to grow and also give back - to ensure that altruistic industry succeeds.
                  </p>
                </div>
              </div>
            </div>
            <div className="pt-6 w-full md:w-4/12 px-4 text-center xs:hidden">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                <div className="px-4 py-5 flex-auto">
                  <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                    <i className="fas fa-rocket"></i>
                  </div>

                  <h6 className="text-xl font-semibold">
                    Industry Standard Technology
                  </h6>
                  <p className="mt-2 mb-4 text-gray-600">
                    Scalable, secure and fast MERN stack. Blazing fast front-end
                    so you never lose a client again.
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* We code. Beautifully. */}
          <div className="flex flex-wrap items-center md:mt-32 xs:mt-4">
            <div className="w-full md:w-5/12 px-4 mx-auto">
              <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-gray-100 xs:ml-[40%]">
                <i className="fas fa-code text-2xl"></i>
              </div>
              <h3 className="text-3xl mb-2 font-semibold leading-normal">
                We code. Beautifully.
              </h3>
              <p className="text-lg font-light leading-relaxed mt-4 mb-4 text-gray-700">
                End-to-end digital solution for your business - never have to
                worry about development again.
              </p>
              <details>
                <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                  <span>See our services</span>
                </summary>
                <div className="ml-2">
                  <details open className="my-4">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>MERN Stack Development</span>
                    </summary>

                    <p className="mt-4 my-4">
                      Using cutting-edge technology standards of the MERN stack,
                      we will deliver the best for you. Don't settle for
                      anything less than a scalable, stable, and blazing-fast
                      app or website.
                    </p>
                  </details>
                  <details>
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Technology Consulting</span>
                    </summary>

                    <p className="mt-4 my-4">
                      We offer consulting for small companies and companies
                      whose operations are not primarily in IT, to ensure you
                      make the right technical decision before it’s too late.
                    </p>
                  </details>
                  <details>
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Startup Stack</span>
                    </summary>

                    <p className="mt-4 my-4">
                      For startups and innovators wanting to test the market and
                      their product we deliver less scalable but more affordable
                      software ready for rapid testing and pivoting.
                    </p>
                  </details>
                  <details className="mb-8">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Jamstack Websites</span>
                    </summary>

                    <p className="mt-4 my-4">
                      If your website needs are mostly a static website,
                      Jamstack offers blazing-fast and secure websites.
                    </p>
                  </details>
                  <details className="my-4">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Perfect for altruistic businesses.</span>
                    </summary>

                    <p className="mt-4 my-4">
                      You don’t only get the perfect website/app for your
                      company. When working with us, you help the altruistic business
                      ecosystem even more.
                    </p>
                  </details>
                 
                </div>
              </details>

              <a
                href="/#contact-form-navigate"
                className="font-bold text-gray-800 mt-8 underline"
              >
                Contact us for more info!
              </a>
            </div>
            <div className="w-full md:w-4/12 px-4 mr-auto ml-auto">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-pink-600">
                <img
                  alt="..."
                  src="https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1051&amp;q=80"
                  className="w-full align-middle rounded-t-lg"
                />
                <blockquote className="relative p-8 mb-4">
                  <svg
                    preserveAspectRatio="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 583 95"
                    className="absolute left-0 w-full block"
                    style={{ height: "95px", top: "-94px" }}
                  >
                    <polygon
                      points="-30,95 583,95 583,65"
                      className="text-pink-600 fill-current"
                    ></polygon>
                  </svg>
                  <h4 className="text-xl font-bold text-white">
                    If you can describe it,
                  </h4>
                  <p className="text-md font-light mt-2 text-white">
                    We can build it. Never worry about your development again.
                  </p>
                </blockquote>
              </div>
            </div>
          </div>
          {/*   
          / We code. Beautifully.
          */}

          {/* Why we do it. */}
          <div className="flex flex-wrap items-center md:mt-16 xs:mt-4">
            <div className="w-full md:w-5/12 px-4 mx-auto">
              <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-gray-100 xs:ml-[40%]">
                <i className="fas fa-flask text-2xl"></i>
              </div>
              <h3 className="text-3xl mb-2 font-semibold leading-normal">
                Our problems are urgent.
              </h3>
              <p className="text-lg font-light leading-relaxed mt-4 mb-4 text-gray-700">
                We are aware that our world has problems that needs to be solved as soon as possible.
               That is why we want to accelerate the altruistic industry and advocacy for it as much as we can.
              </p>
              <details>
                <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                  <span>Learn more</span>
                </summary>
                <div className="ml-2">
                  <details open className="my-4">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Why give back?</span>
                    </summary>

                    <p className="mt-4 my-4">
                      We believe that we all need to unite against the tyranny
                      of diseases, hunger, and other threats to our world. Help must not be given only by charities, entrepreneurs and governments, but also the web developers, artists, lawyers -
                      everyone. We can all help in our own way, and this is how we can help the best.
                    </p>
                  </details>
                  <details>
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>But what about the sweet, sweet profit?</span>
                    </summary>

                    <p className="mt-4 my-4">
                      We don’t really hate yachts, but we want to do good first
                      and show the world and other entrepreneurs the better way
                      - that you can make a profit while also ensuring that the
                      future of our planet is bright and secure. There’s time for
                      everything, and now is the time to fix some pressing
                      issues of mankind. Solve the problem of Ai safety, world hunger and aging first, then
                      solve the problem of buying a Porsche.
                    </p>
                  </details>
                  <details>
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>
                        Why not go with the non-profit model all the way?
                      </span>
                    </summary>

                    <p className="mt-4 my-4">
                      We adopt this hybrid business model because we believe
                      that the competitive advantage of companies that operate
                      on a pure for-profit basis is extremely difficult to beat.
                      For-profit companies have better funding than non-profit
                      ones, and that is because most investors want to have a
                      return on their investment, which isn’t possible in
                      non-profits. We also want to scale and produce even more
                      value, which would be nearly impossible with a pure
                      non-profit model. Hence the hybrid, public-benefit,
                      not-totally-for-profit model.
                    </p>
                  </details>
                  {/* <details className="my-4">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>
                        All of this sounds much like communist propaganda to
                        me...
                      </span>
                    </summary>

                    <p className="mt-4 my-4">Whatever, dude.</p>
                  </details> */}
                </div>
              </details>

              <a
                href="/#contact-form-navigate"
                className="font-bold text-gray-800 mt-8 underline"
              >
                Contact us for more info!
              </a>
            </div>
            <div className="w-full md:w-4/12 px-4 mr-auto ml-auto">
              <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-pink-600">
                <img
                  alt="Lab Research"
                  src="/img/research.jpg"
                  className="w-full align-middle rounded-t-lg"
                />
                <blockquote className="relative p-8 mb-4">
                  <svg
                    preserveAspectRatio="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 583 95"
                    className="absolute left-0 w-full block"
                    style={{ height: "95px", top: "-94px" }}
                  >
                    <polygon
                      points="-30,95 583,95 583,65"
                      className="text-pink-600 fill-current"
                    ></polygon>
                  </svg>
                  <h4 className="text-xl font-bold text-white">
                    Yachts come second,
                  </h4>
                  <p className="text-md font-light mt-2 text-white">
                    Solving AI safety and world hunger come first.
                  </p>
                </blockquote>
              </div>
            </div>

            <>
              {/* <div className="w-full md:w-4/12 px-4 mr-auto ml-auto">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-pink-600">
                <img
                  alt="..."
                  src="https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1051&amp;q=80"
                  className="w-full align-middle rounded-t-lg"
                />
                <blockquote className="relative p-8 mb-4">
                  <svg
                    preserveAspectRatio="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 583 95"
                    className="absolute left-0 w-full block"
                    style={{ height: "95px", top: "-94px" }}
                  >
                    <polygon
                      points="-30,95 583,95 583,65"
                      className="text-pink-600 fill-current"
                    ></polygon>
                  </svg>
                  <h4 className="text-xl font-bold text-white">
                    If you can describe it,
                  </h4>
                  <p className="text-md font-light mt-2 text-white">
                    We can build it. Never worry about your development again.
                  </p>
                </blockquote>
              </div>
            </div> */}
            </>
          </div>
          {/* 
          / Why we do it. 
          */}
        </div>
      </section>
    </StandardPageLayout>
  );
};

export default AboutUsPage;