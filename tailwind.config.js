module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx,html}"],
  theme: {
    screens: {
      //"raw:", as shown on the next line, is how you define custom breakpoints, with min-max, height, max-width, etc.
      // xs: {'raw': '(max-height: 1234px),(min-width:920px)'},
      xs: { max: "639px" },
      "md-max": { max: "768px" },
      // => @media (max-width: 1535px) { ... }
      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      xxl: "1536px",
      // => @media (min-width: 1536px) { ... }
    },

    // colors: {
    //   blue: {
    //     100: '#1fb6ff',
    //     200: '#1fb6ff',
    //     500: '#1fb6ff'
    //   },
    //   purple: '#7e5bef',
    //   pink: '#ff49db',
    //   orange: '#ff7849',
    //   green: '#13ce66',
    //   yellow: '#ffc82c',
    //   grayDark: '#273444',
    //   gray: '#ff0000',
    //   grayLight: '#d3dce6',
    // },
    extend: {
      // screens: {
      //   'xs': '350px',
      // }
    },
  },
  plugins: [],
};
