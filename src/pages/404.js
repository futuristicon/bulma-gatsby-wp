import * as React from "react";
import StandardPageLayout from '../templates/standardPageLayout'

const NotFoundPage = () => (
<StandardPageLayout>
<div class="max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
    <div class="mx-auto space-y-8 max-w-prose">
      <h1>Howdy pardner, you might be lost!</h1>
      <p>You found the page that... wasn't found (Error 404 - Page not Found)
Go back to homepage (clicky clicky on the nav menu above).
If you are sure that there should be a page here, please let our naughty web developer know about their mistake:

      </p>
      <div><a
        className="inline-block px-5 py-3 mt-8 text-sm font-medium text-white bg-blue-500 rounded-lg"
        href="mailto:contact@futuristicon.com?subject=404 page - something is wrong! (description in the mail)"
      >
        Let the naughty dev know
      </a></div>
    </div>
    </div>
    </StandardPageLayout>
);

export default NotFoundPage;
