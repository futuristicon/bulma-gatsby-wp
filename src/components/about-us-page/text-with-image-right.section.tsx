// import { string } from "prop-types";
// import React from "react";

// interface ImageWithTextLeftProps {
//     img: string,

// }

// const ImageWithTextLeft: React.FC<ImageWithTextLeftProps>  = (props) = {
//     ) => {
//   return (
//     <section className="relative flex flex-wrap sm:mb-1 mb-1 h-max">
//       <div className="flex items-center w-full px-4 py-12 lg:w-1/2 sm:px-6 lg:px-8 sm:py-16 lg:py-12 lg:items-center">
//         <div className="max-w-lg mx-auto lg:text-left lg:pr-12">
//           <h2 className="text-3xl font-bold text-gray-900 sm:text-4xl w-[95%] mb-5">
//             What We Do {ImageWithTextLeftProps.img}
//           </h2>

//           <p className="mb-2 mt-4">We develop awesome websites and apps.</p>
//           <p className="my-2">
//             {" "}
//             You can choose whether you want to get involved in technology and
//             design decision-making, or leave everything to us and get a complete
//             digital solution for your needs.{" "}
//           </p>
//           <p className="my-2">
//             Our mission is to build a fast, beautiful, scalable and secure
//             website/application for you.
//           </p>
//           <p className="mb-2 mt-4">
//             For businesses with proven products, we offer fast, scalable,
//             SEO-friendly, and secure websites and mobile apps built using
//             cutting-edge technologies. A beautiful design and on-site SEO are
//             included. You simply get the finished digital solution for your
//             entire business.
//           </p>
//         </div>
//       </div>

//       <div className="relative w-full md:h-64 sm:h-96 lg:w-1/2 lg:h-4/6 my-auto">
//         <img
//           className="inset-0 object-cover w-full lg:h-[380px] lg:w-auto lg:rounded-lg"
//           src="https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
//           alt="Four hands care about the planet"
//         />
//       </div>
//     </section>
//   );
// };
// export default ImageWithTextLeft;
