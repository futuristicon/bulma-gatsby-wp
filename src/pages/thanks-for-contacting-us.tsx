import React from 'react'

import NavigationMenu from '../components/navigation/navigation'
import Footer from '../components/footer/footer'

import StandardPageLayout from '../templates/standardPageLayout'


const Homepage = () => {
return (
  <StandardPageLayout>
    <div
      className="relative pt-16 pb-32 flex content-center items-center justify-center"
      style={{ minHeight: "75vh" }}
    >
      <div
        className="absolute top-0 w-full h-full bg-center bg-cover"
        style={{
          backgroundImage: `url("/img/hero-index-page.resized.min.jpeg")`,
        }}
      >
        <span
          id="blackOverlay"
          className="w-full h-full absolute opacity-75 bg-black"
        ></span>
      </div>
      <div className="container relative mx-auto">
        <div className="items-center flex flex-wrap">
          <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
            <div className="sm:mr-12 lg:pr-12">
              <h1 className="text-white font-semibold text-5xl">
                Message sent!{" "}
              </h1>
              <p className="mt-4 text-lg text-gray-300">
                Thank you for contacting us. We will reply very soon. <br />{" "}
                <a href="/" className="underline">
                  Back to homepage?
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div
        className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
        style={{ height: "70px" }}
      >
        <svg
          className="absolute bottom-0 overflow-hidden"
          xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="none"
          version="1.1"
          viewBox="0 0 2560 100"
          x="0"
          y="0"
        >
          <polygon
            className="text-gray-300 fill-current"
            points="2560 0 2560 100 0 100"
          ></polygon>
        </svg>
      </div>
    </div>
    <section className="pb-20 bg-gray-300 -mt-24">
      <div className="container mx-auto px-4">
        <div className="flex flex-wrap">
          <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
              <div className="px-4 py-5 flex-auto">
                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                  <i className="fas fa-award"></i>
                </div>
                <h6 className="text-xl font-semibold">
                  Taking Care of Business
                </h6>
                <p className="mt-2 mb-4 text-gray-600">
                  End-to-end digital solution for your business - never have to
                  worry about development again.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-4/12 px-4 text-center">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
              <div className="px-4 py-5 flex-auto">
                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                  <i className="fas fa-rocket"></i>
                </div>
                <h6 className="text-xl font-semibold">
                  Industry Standard Technology
                </h6>
                <p className="mt-2 mb-4 text-gray-600">
                  Scalable, secure and fast MERN stack. Blazing fast front-end
                  so you never lose a client again.
                </p>
              </div>
            </div>
          </div>
          <div className="pt-6 w-full md:w-4/12 px-4 text-center">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
              <div className="px-4 py-5 flex-auto">
                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                  <i className="fas fa-leaf"></i>
                </div>
                <h6 className="text-xl font-semibold">
                  Perfect for Your Company{" "}
                  <span>
                    <a href="/about-us">
                      <i className="far fa-question-circle"></i>
                    </a>
                  </span>
                </h6>
                <p className="mt-2 mb-4 text-gray-600">
                Our hybrid business model allows us to grow and also give back - to ensure that altruistic industry succeeds.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </StandardPageLayout>
);
}

export default Homepage





