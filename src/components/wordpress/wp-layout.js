import React from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import parse from "html-react-parser"

const Layout = ({ isHomePage, children }) => {
  const {
    wp: {
      generalSettings: { title },
    },
  } = useStaticQuery(graphql`
    query LayoutQuery {
      wp {
        generalSettings {
          title
          description
        }
      }
    }
  `)

  return (
    <div className="global-wrapper" data-is-root-path={isHomePage}>
      <header className="global-header">
        {isHomePage ? (
<>          <h1 className="main-heading">
            <Link to="/personal-blog">{parse(title)} blog</Link>
          </h1>
          <Link className="header-link-home" to="/">
            Go to {title} homepage
          </Link>
          </>
        ) : (
          <><Link className="header-link-home" to="/">
            Go to {title} homepage
          </Link>
          <br/>
          <Link className="header-link-home" to="/personal-blog">
          See all posts
        </Link></>
        )}
      </header>

      <main>{children}</main>

      <footer>
        Futuristicon© {new Date().getFullYear()}</footer>
    </div>
  )
}

export default Layout
