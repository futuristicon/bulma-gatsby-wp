import React from 'react'
import { Helmet } from "react-helmet";
import { withPrefix } from "gatsby";

import NavigationMenu from "../components/navigation/navigation";
import Footer from "../components/footer/footer";

const StandardPageLayout = (props: any) => {
  const cacheInvalidationVersion = "?v1.2";

  return (
    <>
      <Helmet>
        <title>
          {props.title ||
            "Futuristicon - Development for Altruistic Companies and Startups"}
        </title>
        <meta
          name="description"
          content={
            props.description ||
            "We offer developing services for companies tackling most pressing issues of our world, and also give back to solving those problems."
          }
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={`${withPrefix(
            "/"
          )}img/apple-touch-icon.png${cacheInvalidationVersion}`}
        />
        <link
          rel="icon"
          type="image/png"
          href={`${withPrefix(
            "/"
          )}img/favicon-32x32.png${cacheInvalidationVersion}`}
          sizes="32x32"
        />
        <link
          rel="icon"
          type="image/png"
          href={`${withPrefix(
            "/"
          )}img/favicon-16x16.png${cacheInvalidationVersion}`}
          sizes="16x16"
        />
        <meta
          property="og:title"
          content={
            props.title ||
            "Futuristicon - Development for Longevity Companies and Startups"
          }
        />
        <meta
          property="og:image"
          content={`${withPrefix("/")}img/og-image.jpg`}
        />
      </Helmet>
      <div className="text-gray-800 antialiased">
        <NavigationMenu />
        {props.children}
        <Footer />
      </div>
    </>
  );
};

export default StandardPageLayout