import React from "react";

import StandardPageLayout from "../templates/standardPageLayout";
import ContactForm from "../components/contact-form/contact-form";
import SubNavigationSection from "../sections/sub-navigation.section";

const Homepage = () => {
  return (
    <StandardPageLayout>
      <>
        <main>
          <SubNavigationSection
            heading="We Develop for Altruistic Companies"
            subHeading="And we invest 40% of our profit to giving you a louder voice.
            "
          />
          <section className="pb-20 bg-gray-300 -mt-24">
            <div className="container mx-auto px-4">
              <div className="flex flex-wrap">
                <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                        <i className="fas fa-award"></i>
                      </div>
                      <h6 className="text-xl font-semibold">
                        Taking Care of Business
                      </h6>
                      <p className="mt-2 mb-4 text-gray-600">
                        End-to-end digital solution for your business - never
                        have to worry about development again.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="w-full md:w-4/12 px-4 text-center">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400">
                        <i className="fas fa-rocket"></i>
                      </div>
                      <h6 className="text-xl font-semibold">
                        Industry Standard Technology
                      </h6>
                      <p className="mt-2 mb-4 text-gray-600">
                        Scalable, secure and fast MERN stack. Blazing fast
                        front-end so you never lose a client again.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="pt-6 w-full md:w-4/12 px-4 text-center">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400">
                        <i className="fas fa-leaf"></i>
                      </div>
                      <h6 className="text-xl font-semibold">
                        Perfect for Your Company{" "}
                        <span>
                          <a href="/about-us">
                            <i className="far fa-question-circle"></i>
                          </a>
                        </span>
                      </h6>
                      <p className="mt-2 mb-4 text-gray-600">
                      Our hybrid business model allows us to grow and also give back - to ensure that altruistic industry succeeds.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex flex-wrap items-center md:mt-32 xs:mt-4">
                <div className="w-full md:w-5/12 px-4 mx-auto">
                  <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-gray-100 xs:ml-[40%]">
                    <i className="fas fa-puzzle-piece text-2xl pl-[0.3rem] pb-[0.3rem]"></i>
                  </div>
                  <h3 className="text-3xl mb-2 font-semibold leading-normal">
                    We are the right fit.
                  </h3>
                  <p className="text-lg font-light leading-relaxed mt-4 mb-4 text-gray-700">
                    No matter the size of your company, we have a solution for
                    you.
                  </p>
                  <details open className="my-4">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>MERN Stack Development</span>
                    </summary>

                    <p className="mt-4 my-4">
                      Using cutting-edge technology standards of the MERN stack,
                      we will deliver the best for you. Don't settle for
                      anything less than a scalable, stable, and blazing-fast
                      app or website.
                    </p>
                  </details>
                  <details>
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Technology Consulting</span>
                    </summary>

                    <p className="mt-4 my-4">
                      We offer consulting for small companies and companies
                      whose operations are not primarily in IT, to ensure you
                      make the right technical decision before it’s too late.
                    </p>
                  </details>
                  <details>
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Startup Stack</span>
                    </summary>

                    <p className="mt-4 my-4">
                      For startups and innovators wanting to test the market and
                      their product we deliver less scalable but more affordable
                      software ready for rapid testing and pivoting.
                    </p>
                  </details>
                  <details className="my-4">
                    <summary className="text-xl font-bold leading-tight text-gray-900 mb-4 hover:cursor-pointer">
                      <span>Perfect for altruistic businesses.</span>
                    </summary>

                    <p className="mt-4 my-4">
                      You don’t only get the perfect website/app for your
                      company. When working with us, you help the altruistic business
                      ecosystem even more.
                    </p>
                  </details>

                  <a
                    href="#contact-form-navigate"
                    className="font-bold text-gray-800 mt-8 underline"
                  >
                    Contact us for more info!
                  </a>
                </div>
                <div className="w-full md:w-4/12 px-4 mr-auto ml-auto">
                  <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-pink-600">
                    <img
                      alt="..."
                      src="https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1051&amp;q=80"
                      className="w-full align-middle rounded-t-lg"
                    />
                    <blockquote className="relative p-8 mb-4">
                      <svg
                        preserveAspectRatio="none"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 583 95"
                        className="absolute left-0 w-full block"
                        style={{ height: "95px", top: "-94px" }}
                      >
                        <polygon
                          points="-30,95 583,95 583,65"
                          className="text-pink-600 fill-current"
                        ></polygon>
                      </svg>
                      <h4 className="text-xl font-bold text-white">
                        If you can describe it,
                      </h4>
                      <p className="text-md font-light mt-2 text-white">
                        We can build it. Never worry about your development
                        again.
                      </p>
                    </blockquote>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <>
            {/* <section className="relative py-20">
              <div
                className="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20"
                style={{ height: "80px" }}
              >
                <svg
                  className="absolute bottom-0 overflow-hidden"
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
                >
                  <polygon
                    className="text-white fill-current"
                    points="2560 0 2560 100 0 100"
                  ></polygon>
                </svg>
              </div>
              <div className="container mx-auto px-4">
                <div className="items-center flex flex-wrap">
                  <div className="w-full md:w-4/12 ml-auto mr-auto px-4">
                    <img
                      alt="..."
                      className="max-w-full rounded-lg shadow-lg"
                      src="https://images.unsplash.com/photo-1555212697-194d092e3b8f?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=634&amp;q=80"
                    />
                  </div>
                  <div className="w-full md:w-5/12 ml-auto mr-auto px-4">
                    <div className="md:pr-12">
                      <div className="text-pink-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-pink-300">
                        <i className="fas fa-rocket text-xl"></i>
                      </div>
                      <h3 className="text-3xl font-semibold">
                        A growing company
                      </h3>
                      <p className="mt-4 text-lg leading-relaxed text-gray-600">
                        The extension comes with three pre-built pages to help
                        you get started faster. You can change the text and
                        images and you're good to go.
                      </p>
                      <ul className="list-none mt-6">
                        <li className="py-2">
                          <div className="flex items-center">
                            <div>
                              <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 bg-pink-200 mr-3">
                                <i className="fas fa-leaf"></i>
                              </span>
                            </div>
                            <div>
                              <h4 className="text-gray-600">
                                Carefully crafted components
                              </h4>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="flex items-center">
                            <div>
                              <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 bg-pink-200 mr-3">
                                <i className="fab fa-html5"></i>
                              </span>
                            </div>
                            <div>
                              <h4 className="text-gray-600">
                                Amazing page examples
                              </h4>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="flex items-center">
                            <div>
                              <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 bg-pink-200 mr-3">
                                <i className="far fa-paper-plane"></i>
                              </span>
                            </div>
                            <div>
                              <h4 className="text-gray-600">
                                Dynamic components
                              </h4>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section> */}
          </>
          {/* Team Section Goes Here */}

          <section className="pb-20 relative block bg-gray-900 ">
            <div
              className="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20"
              style={{ height: "80px" }}
            >
              <svg
                className="absolute bottom-0 overflow-hidden"
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="text-gray-900 fill-current"
                  points="2560 0 2560 100 0 100"
                ></polygon>
              </svg>
            </div>
            <div className="container mx-auto px-4 lg:pt-24 lg:pb-64">
              <div className="flex flex-wrap text-center justify-center">
                <div
                  className="w-full lg:w-6/12 px-4"
                  id="contact-form-navigate"
                >
                  <h2 className="text-4xl font-semibold text-white">
                    Let's begin
                  </h2>
                  <p className="text-lg leading-relaxed mt-4 mb-4 text-gray-500">
                    Let's grow together by beginning a wonderful partnership:
                  </p>
                </div>
              </div>
            </div>
          </section>
          <section className="relative block py-24 lg:pt-0 bg-gray-900">
            <div className="container mx-auto px-4  xs:pt-4">
              <div className="flex flex-wrap justify-center lg:-mt-64 -mt-48">
                <div className="w-full lg:w-6/12 px-4">
                  <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300">
                    <ContactForm />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </>
    </StandardPageLayout>
  );
};

export default Homepage;

// import React from "react";
// import { Helmet } from "react-helmet"

// import StandardPageLayout from "../templates/standardPageLayout";

// const Homepage = () => {
//   return (
//     <StandardPageLayout>
//       <>
//         {/* Hands and planet */}
//         <section className="relative flex flex-wrap lg:mb-20 sm:mb-1">
//           <div className="flex items-center w-full px-4 py-12  lg:w-1/2 sm:px-6 lg:px-8 sm:py-16 lg:py-24 lg:h-screen lg:items-center">
//             <div className="max-w-lg mx-auto text-center lg:text-left lg:pr-12">
//               <h2 className="text-3xl font-bold text-gray-900 sm:text-4xl">
//                 Build your business and advance medicine while doing it.
//               </h2>

//               <p className="mt-4 text-gray-500">
//                 We will design and develop the perfect website/app for your
//                 business, while 40% of our profit will go to solving medical and
//                 environmental problems.
//               </p>

//               <p className="mt-4 text-gray-400">
//                 Ask about the{" "}
//                 <span
//                 //@todo make hover rainbow color for string "magical deal"
//                 // style={{
//                 //   background:
//                 //     "-webkit-linear-gradient(90deg, rgba(255,0,0,1) 0%, rgba(255,154,0,1) 10%, rgba(208,222,33,1) 20%, rgba(79,220,74,1) 30%, rgba(63,218,216,1) 40%, rgba(47,201,226,1) 50%, rgba(28,127,238,1) 60%, rgba(95,21,242,1) 70%, rgba(186,12,248,1) 80%, rgba(251,7,217,1) 90%, rgba(255,0,0,1) 100%)",
//                 //   webkitBackgroundClip: "text",
//                 //   webkitTextFillColor: "transparent",
//                 // }}
//                 >
//                   magical deal
//                 </span>{" "}
//                 if you are a medical/biotech startup.
//               </p>

//               <div className="flex xs:justify-center sm:justify-center lg:justify-start ">
//                 <a
//                   className="inline-block px-5 py-3 mt-8 text-sm font-medium text-white bg-blue-500 rounded-lg "
//                   style={{ minWidth: "9em" }}
//                   href="/about-us"
//                 >
//                   Find out more
//                 </a>
//                 <a
//                   className="inline-block px-5 py-3 mt-8 text-sm font-medium text-white rounded-lg ml-2 "
//                   style={{ backgroundColor: "#F6A834", minWidth: "9em" }}
//                   href="/contact-us"
//                 >
//                   Partner Up
//                 </a>
//               </div>
//             </div>
//           </div>

//           <div className="relative w-full h-64 sm:h-96 lg:w-1/2 lg:h-auto">
//             <img
//               style={{ borderRadius: "4px" }}
//               className="absolute inset-0 object-cover w-full h-full"
//               src="https://images.pexels.com/photos/7048012/pexels-photo-7048012.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
//               alt="Four hands care about the planet"
//             />
//           </div>
//         </section>
//       </>
//     </StandardPageLayout>
//   );
// };

// export default Homepage;

/* Team Section */
/* <section className="pt-20 pb-48">
            <div className="container mx-auto px-4">
              <div className="flex flex-wrap justify-center text-center mb-24">
                <div className="w-full lg:w-6/12 px-4">
                  <h2 className="text-4xl font-semibold">
                    Here are our heroes
                  </h2>
                  <p className="text-lg leading-relaxed m-4 text-gray-600">
                    According to the National Oceanic and Atmospheric
                    Administration, Ted, Scambos, NSIDClead scentist, puts the
                    potentially record maximum.
                  </p>
                </div>
              </div>
              <div className="flex flex-wrap">
                <div className="w-full md:w-6/12 lg:w-3/12 lg:mb-0 mb-12 px-4">
                  <div className="px-6">
                    <img
                      alt="..."
                      src="/img/team-1-800x800.jpg"
                      className="shadow-lg rounded-full max-w-full mx-auto"
                      style={{ maxWidth: "120px" }}
                    />
                    <div className="pt-6 text-center">
                      <h5 className="text-xl font-bold">Ryan Tompson</h5>
                      <p className="mt-1 text-sm text-gray-500 uppercase font-semibold">
                        Web Developer
                      </p>
                      <div className="mt-6">
                        <button
                          className="bg-blue-400 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-twitter"></i>
                        </button>
                        <button
                          className="bg-blue-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-facebook-f"></i>
                        </button>
                        <button
                          className="bg-pink-500 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-dribbble"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="w-full md:w-6/12 lg:w-3/12 lg:mb-0 mb-12 px-4">
                  <div className="px-6">
                    <img
                      alt="..."
                      src="/img/team-2-800x800.jpg"
                      className="shadow-lg rounded-full max-w-full mx-auto"
                      style={{ maxWidth: "120px" }}
                    />
                    <div className="pt-6 text-center">
                      <h5 className="text-xl font-bold">Romina Hadid</h5>
                      <p className="mt-1 text-sm text-gray-500 uppercase font-semibold">
                        Marketing Specialist
                      </p>
                      <div className="mt-6">
                        <button
                          className="bg-red-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-google"></i>
                        </button>
                        <button
                          className="bg-blue-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-facebook-f"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="w-full md:w-6/12 lg:w-3/12 lg:mb-0 mb-12 px-4">
                  <div className="px-6">
                    <img
                      alt="..."
                      src="/img/team-3-800x800.jpg"
                      className="shadow-lg rounded-full max-w-full mx-auto"
                      style={{ maxWidth: "120px" }}
                    />
                    <div className="pt-6 text-center">
                      <h5 className="text-xl font-bold">Alexa Smith</h5>
                      <p className="mt-1 text-sm text-gray-500 uppercase font-semibold">
                        UI/UX Designer
                      </p>
                      <div className="mt-6">
                        <button
                          className="bg-red-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-google"></i>
                        </button>
                        <button
                          className="bg-blue-400 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-twitter"></i>
                        </button>
                        <button
                          className="bg-gray-800 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-instagram"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="w-full md:w-6/12 lg:w-3/12 lg:mb-0 mb-12 px-4">
                  <div className="px-6">
                    <img
                      alt="..."
                      src="/img/team-4-470x470.png"
                      className="shadow-lg rounded-full max-w-full mx-auto"
                      style={{ maxWidth: "120px" }}
                    />
                    <div className="pt-6 text-center">
                      <h5 className="text-xl font-bold">Jenna Kardi</h5>
                      <p className="mt-1 text-sm text-gray-500 uppercase font-semibold">
                        Founder and CEO
                      </p>
                      <div className="mt-6">
                        <button
                          className="bg-pink-500 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-dribbble"></i>
                        </button>
                        <button
                          className="bg-red-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-google"></i>
                        </button>
                        <button
                          className="bg-blue-400 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-twitter"></i>
                        </button>
                        <button
                          className="bg-gray-800 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                        >
                          <i className="fab fa-instagram"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section> */
/* / Team Section */
